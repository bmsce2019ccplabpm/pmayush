#include <stdio.h>
int main()
{
    char c;
    int Lowercasevowel, Uppercasevowel;
    printf("Enter an alphabet: ");
    scanf("%c",&c);
   
    Lowercasevowel = (c == 'a' || c == 'e' || c == 'i' || c == 'o' || c == 'u');
   
    uppercasevowel = (c == 'A' || c == 'E' || c == 'I' || c == 'O' || c == 'U');
    
    if (Lowercasevowel || uppercasevowel)
        printf("%c is a vowel.", c);
    else
        printf("%c is a consonant.", c);
    return 0;
}